package fr.dawan.javaiterm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.time.Duration;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CalculTest {

    @BeforeAll // @BeforeAll -> méthode statique executer avant toutes les méthodes de tests
    public static void setup() {
        System.out.println("setup");
    }

    @BeforeEach // @BeforeEach -> méthode executer avant chaque méthode de test
    public void before() {
        System.out.println("BeforeEach");
    }

    @AfterEach() // @AfterEach -> méthode executer aprés chaque méthode de test
    public void after() {
        System.out.println("AfterEach");
    }

    @AfterAll() // @AfterAll -> méthodestatique executer apréstous les méthodes de test
    public static void teardown() {
        System.out.println("AfterAll");
    }

    @Test // @Test -> Méthode de test
    @DisplayName("Le test de la méthode somme") // @DisplayName -> renommer le test
    public void testSomme() {
        assertEquals(3, Calcul.somme(1, 2));
    }

    @Test
    public void testMultiplication() {
        assertEquals(6, Calcul.multiplication(2, 3));
        assertEquals(0, Calcul.multiplication(0, 3));
        assertEquals(-3, Calcul.multiplication(-1, 3));
    }

    @Test
    public void testDivision() {
        assertEquals(5, Calcul.division(10, 2));
//        try {
//            Calcul.division(2, 0);
//            assertTrue(false);
//        }
//        catch(ArithmeticException e) {
//            assertTrue(true);
//        }
        assertThrows(ArithmeticException.class, () -> Calcul.division(1, 0));

    }

    @Test
    public void testTraitement() {
        assertTimeout(Duration.ofMillis(600), Calcul::traitement);
    }
}
