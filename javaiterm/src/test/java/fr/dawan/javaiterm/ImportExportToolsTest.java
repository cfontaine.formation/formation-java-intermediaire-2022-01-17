package fr.dawan.javaiterm;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

public class ImportExportToolsTest {

    @Test
    public void testToBin() {
        Produit p = new Produit("Test", 42.0);
        try {
            ImportExportTools.toBin("test.bin", p);
            Produit p2 = ImportExportTools.fromBin("test.bin");
            assertEquals(p.getDescription(), p2.getDescription());
            assertEquals(p.getPrix(), p2.getPrix());
//            assertEquals(p.toString(), p2.toString());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void testToCvs() {
        List<Produit> lp = Arrays.asList(new Produit("Ordi", 300), new Produit("clavier", 40));
        try {
            ImportExportTools.toCsv("test.csv",lp);
            List<Produit> lp2 = ImportExportTools.fromCsv("test.csv", Produit.class); 
            assertEquals(lp2.toString(), lp.toString());
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }
    
    @AfterAll
    public static void clean() {
        File fp=new File("test.bin");
        fp.delete();
        fp=new File("test.csv");
        fp.delete();
    }
}
