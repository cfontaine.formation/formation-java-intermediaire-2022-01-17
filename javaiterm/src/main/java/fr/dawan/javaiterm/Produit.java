package fr.dawan.javaiterm;

import java.io.Serializable;

public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    private String description;
    private double prix;

    public Produit() {

    }

    public Produit(String description, double prix) {
        this.description = description;
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Produit [description=" + description + ", prix=" + prix + "]";
    }

}
