package fr.dawan.javaiterm.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.dawan.javaiterm.ImportExportTools;
import fr.dawan.javaiterm.Produit;
import fr.dawan.javaiterm.generiques.Paire;

public class MainGenerique 
{
    public static void main( String[] args )
    {
        Paire<Integer,String> p1= new Paire<>(123,"AZERTY");
        System.out.println(p1.getF() + " " + p1.getS());
        
        Paire<Double,Double > p2 = new Paire<>(1.23,4.56);
        System.out.println(p2.getF() + " " + p2.getS());
        
        System.out.println(Paire.<Integer>isEqual(1,2));
        System.out.println(Paire.<String>isEqual("1","1"));
        
        
        Produit produit1=new Produit("Ecran 4K 60 pouces",900.0);
        Produit produit2=null;
        try {
           ImportExportTools.toBin("produit.bin", produit1);
            produit2=ImportExportTools.fromBin("produit.bin");
            System.out.println(produit2);
        } catch (IOException e) {
              e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        // Générique WildCard ?
        List<Produit> lstP=new ArrayList<>();
        lstP.add(produit1);
        // List<Object> lstO =lstP;
        List<?> lstW=lstP;
        //clstW.add(new Double(4.5));
        System.out.println(lstW.get(0));
        
        // Contrainte  sur ?
        List<Integer> lstInt=Arrays.asList(2,6,1,8);
        List<? extends Number> lstNum=  lstInt; // lstP -> erreur
        List<? super Integer> lstSup=lstInt;
    }
}
