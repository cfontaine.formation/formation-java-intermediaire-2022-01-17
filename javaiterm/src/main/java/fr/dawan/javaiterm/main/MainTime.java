package fr.dawan.javaiterm.main;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class MainTime {

    public static void main(String[] args) {
       LocalDate d1= LocalDate.now();
       System.out.println(d1);
       
       LocalDate d2=LocalDate.of(2022,Month.JULY,14);
       System.out.println(d2);
    
       System.out.println(d1.format(DateTimeFormatter.ofPattern("dd/MM/yy")));
       
       Period per1=Period.between(d1, d2);
       System.out.println(per1);
       
       LocalTime t1=LocalTime.now(ZoneId.of("GMT+3"));
       System.out.println(t1);
    }

}
