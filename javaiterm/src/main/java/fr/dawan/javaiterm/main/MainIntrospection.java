package fr.dawan.javaiterm.main;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.javaiterm.ImportExportTools;
import fr.dawan.javaiterm.Produit;

public class MainIntrospection {

    public static void main(String[] args) {
        Class<?> cStr = null;
        // 1
        // String str= "Hello";
        // cStr=str.getClass();

        // 2
        try {
            cStr = Class.forName("java.lang.String");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // 3
        // Class<String> cStr2= String.class;

        Field[] fStr = cStr.getDeclaredFields();
        for (Field f : fStr) {
            System.out.println(f.getName() + " " + Modifier.isPrivate(f.getModifiers()));
        }

        Class<?> supCStr = cStr.getSuperclass();
        System.out.println(supCStr.getName() + " " + supCStr.getSimpleName());

        try {

            String instCstr = (String) cStr.newInstance();
            Constructor<String> constCStr = ((Class<String>) cStr)
                    .getConstructor(new Class[] { Class.forName("java.lang.String") });

            String constInstCstr = constCStr.newInstance("azeet");
            System.out.println(constInstCstr);

            Method methCToString = ((Class<String>) cStr).getMethod("toString", new Class[] {});
            // methCToString.setAccessible(true);// pour appeler dynamiquement une méthode privée
            System.out.println(methCToString.invoke(constInstCstr, new Object[] {}));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {

            e.printStackTrace();
        } catch (SecurityException e) {

            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        List<Produit> lstP = new ArrayList<>();
        lstP.add(new Produit("Tv 4K", 850.0));
        lstP.add(new Produit("Souris", 19.0));
        lstP.add(new Produit("SSD 1Go", 100.0));
        lstP.add(new Produit("Clavier", 19.0));
        try {
            ImportExportTools.toCsv("produit.csv", lstP);
            List<Produit> lstR= ImportExportTools.fromCsv("produit.csv",Produit.class);
            for(Produit p : lstR)
            {
                System.out.println(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }

}
