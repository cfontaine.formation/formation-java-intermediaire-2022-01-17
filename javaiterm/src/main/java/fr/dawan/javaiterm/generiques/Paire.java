package fr.dawan.javaiterm.generiques;

public class Paire<T,S> {
    private T f;
    private S s;
    
    

    public Paire(T f, S s) {
        this.f = f;
        this.s = s;
    }



    public T getF() {
        return f;
    }



    public S getS() {
        return s;
    }

    public static <U> boolean isEqual(U a, U b) {
        return a.equals(b);
    }
 
    
}
