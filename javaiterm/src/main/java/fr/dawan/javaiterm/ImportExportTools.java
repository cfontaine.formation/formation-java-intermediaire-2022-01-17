package fr.dawan.javaiterm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ImportExportTools {

    public static <T extends Serializable> void toBin(String fileName, T o) throws IOException {
        try (BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(fileName))) {
            ObjectOutputStream oss = new ObjectOutputStream(fos);
            oss.writeObject(o);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T fromBin(String fileName) throws IOException, ClassNotFoundException {
        Object o = null;
        try (BufferedInputStream fis = new BufferedInputStream(new FileInputStream(fileName))) {
            ObjectInputStream ois = new ObjectInputStream(fis);
            o = ois.readObject();
        }
        return (T) o;
    }

    public static <T> void toCsv(String fileName, List<T> lst) throws Exception {
        try (BufferedWriter fw = new BufferedWriter(new FileWriter(fileName))) {
            for (T e : lst) {
                Class<?> cElm = e.getClass();
                Field fields[] = cElm.getDeclaredFields();
                StringBuilder sb = new StringBuilder();
                for (Field f : fields) {
                    int modif = f.getModifiers();
                    if (!Modifier.isStatic(modif) && !Modifier.isFinal(modif)) {
                        f.setAccessible(true); // pour les attributs privées
                        sb.append(f.get(e).toString()).append(";");
                    }
                }
                sb.deleteCharAt(sb.length() - 1);
                fw.write(sb.toString());
                fw.newLine();
            }

        }
    }

    public static <T> List<T> fromCsv(String fileName, Class<T> clazz) throws Exception {
        List<T> lp = new ArrayList<T>();
        try (BufferedReader fis = new BufferedReader(new FileReader(fileName))) {

            String ligne;
            while ((ligne = fis.readLine()) != null) {
                String elm[] = ligne.split(";");
                T o = clazz.newInstance();   //création d'une instance générique
                Field[] fields = clazz.getDeclaredFields();
                int i = 0;
                for (Field f : fields) {
                    f.setAccessible(true);
                    switch(f.getType().getSimpleName())
                    {
                    case("String"):
                        f.set(o, elm[i++]);
                    break;
                    case("double"):
                        f.set(o, Double.valueOf(elm[i++]));
                    break;
                    // case ...
                    }
                }
                lp.add(o);
            }

        }
        return lp;
    }
}
