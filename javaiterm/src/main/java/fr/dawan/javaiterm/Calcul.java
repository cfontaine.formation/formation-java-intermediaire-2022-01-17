package fr.dawan.javaiterm;

public class Calcul {

    public static int somme(int a, int b) {
        return a+b;
    }
    
    public static int multiplication(int a, int b) {
        return a*b;
    }
    
    public static int division(int a, int b) {
        return a/b;
    }
    
    public static String traitement() throws InterruptedException {
        Thread.sleep(500);
        return "Traitement Ok";
    }
}
